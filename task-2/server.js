const express = require('express');

const app = express();
const port = 8000;

const Vigenere = require('caesar-salad').Vigenere;

app.get('/encode/:password', (req, res) => {
    const encode = Vigenere.Cipher('c').crypt(req.params.password);
    res.send(encode)
});

app.get('/decode/:password', (req, res) => {
    const decode = Vigenere.Decipher('c').crypt(req.params.password);
    res.send(decode)
});

app.listen(port);